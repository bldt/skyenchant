package co.binarylife.skyregions.skyenchant.enchants.enchants.sword;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class DischargeEnchant extends ToolEnchant {

	public DischargeEnchant() {
		super("Discharge", 2, 0.7);
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player && !event.isCancelled()) {
			Player p = (Player) event.getDamager();
			ItemStack item = p.getItemInHand();
			if (hasEnchant(item)) {
				
				double chance = getChance(0.1);
				
				if (new Random().nextDouble() <= chance)
					p.getWorld().strikeLightning(event.getEntity().getLocation());
			}
		}
	}

	@Override
	public void applyEffect(Player p) {
		// bad design lel
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.SWORD;
	}
	
	

}
