package co.binarylife.skyregions.skyenchant.enchants;

import co.binarylife.skyregions.skyenchant.lang.Lang;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sneling on 19/11/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public enum EnchantmentCategory {
    ALL(Material.COMPASS),

    HELMET(Material.DIAMOND_HELMET),
    BOOTS(Material.DIAMOND_BOOTS),
    ARMOR(Material.DIAMOND_CHESTPLATE),

    WEAPONS(Material.DIAMOND_SWORD),
    BOW(Material.BOW),
    SWORD(Material.DIAMOND_SWORD),

    AXE(Material.DIAMOND_AXE),
    TOOLS(Material.DIAMOND_HOE),
    PICKAXE(Material.DIAMOND_PICKAXE),

    OTHER(Material.COMPASS);

    Material type;
    String displayName;
    List<String> lores = new ArrayList<>();

    EnchantmentCategory(Material type) {
        this.type = type;
        this.displayName = Lang.valueOf("GUI_INFO_ENCHANTS_" + this.toString() + "_DISPLAYNAME", true);

        this.lores = Arrays.asList(Lang.valueOf("GUI_INFO_ENCHANTS_" + this.toString() + "_LORES", true).split("%NL%"));
    }

    public ItemStack toItemStack(){
        ItemStack res = new ItemStack(type);

        ItemMeta meta = res.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setLore(lores);

        res.setItemMeta(meta);

        return res;
    }

}