package co.binarylife.skyregions.skyenchant.enchants.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import co.binarylife.skyregions.skyenchant.SkyEnchant;
import co.binarylife.skyregions.skyenchant.util.PlayerUtil;

public class PlayerListener implements Listener {
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		SkyEnchant.getInstance().getServer().getScheduler().runTaskLater(SkyEnchant.getInstance(),
				new PlayerUpdateRunnable(event.getPlayer()), 2L);
	}
	
	public static class PlayerUpdateRunnable implements Runnable {
		
		private Player p;
		
		public PlayerUpdateRunnable(Player p) {
			this.p = p;
		}

		@Override
		public void run() {
			PlayerUtil.applyArmorEnchants(p);
		}
		
	}

}
