package co.binarylife.skyregions.skyenchant.enchants.enchants.armor;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ArmorMetadataEnchant;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class VanishEnchant extends ArmorMetadataEnchant {

	public VanishEnchant() {
		super("Vanish", 2, 0.7);
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player)) return;
		Player p = (Player) event.getEntity();
		if (hasEffect(p)) {

			double chance = getChance(.05);
			if (new Random().nextDouble() <= chance)
				((Player) event.getDamager()).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 0, 5));
			
		}
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.ARMOR;
	}

}
