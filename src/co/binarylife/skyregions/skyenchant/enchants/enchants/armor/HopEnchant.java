package co.binarylife.skyregions.skyenchant.enchants.enchants.armor;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ArmorEnchant;

public class HopEnchant extends ArmorEnchant {

	public HopEnchant() {
		super("Hop", 3, 0.8);
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.BOOTS;
	}

	@Override
	public void applyEffect(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 1));
	}

	@Override
	public void removeEffect(Player p) {
		p.removePotionEffect(PotionEffectType.JUMP);
	}

	@Override
	public boolean hasEffect(Player p) {
		return p.hasPotionEffect(PotionEffectType.JUMP);
	}

}
