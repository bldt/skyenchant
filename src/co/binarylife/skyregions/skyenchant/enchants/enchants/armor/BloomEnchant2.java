package co.binarylife.skyregions.skyenchant.enchants.enchants.armor;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;

import co.binarylife.skyregions.skyenchant.SkyEnchant;
import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ArmorMetadataEnchant;

public class BloomEnchant2 extends ArmorMetadataEnchant {

	public BloomEnchant2() {
		super("Bloom 2", 3, 0.7);
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.HELMET;
	}

	@Override
	public void applyEffect(Player p) {
		if (!p.getMetadata(getName()).isEmpty())
			return;

		p.setMaxHealth(p.getMaxHealth() + 6);
		p.setMetadata(getName(), new FixedMetadataValue(SkyEnchant.getInstance(), 2));
//		p.sendMessage("[" + getName() + "] You get 3 extra hearts");
	}

	@Override
	public void removeEffect(Player p) {
		p.setMaxHealth(20);
		p.removeMetadata(getName(), SkyEnchant.getInstance());
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		// Remove bloom bc health is persistent
		if (hasEffect(event.getPlayer())) {
			event.getPlayer().setMaxHealth(20);
			event.getPlayer().removeMetadata(getName(), SkyEnchant.getInstance());
		}
	} 

}
