package co.binarylife.skyregions.skyenchant.enchants;

import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.AquaEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.BeastEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.BloomEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.DashEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.DevourEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.HopEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.IlluminateEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.MagmaEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.QueasyEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.RejuvenateEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.VanishEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.armor.WomboComboEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.axe.HeavyHitsEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.axe.SlugEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.bow.ChainedEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.bow.HeadshotEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.sword.DisappearEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.sword.DischargeEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.sword.GrindEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.sword.LifeStealEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.sword.SickenEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.sword.WitheredEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.tools.BurstEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.tools.ObsidianDestroyerEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.tools.SwiftEnchant;

/**
 * Created by Sneling on 19/11/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class SkyEnchants {

	// Armor
	public static final MagmaEnchant MAGMA_ENCHANT = new MagmaEnchant();
	public static final AquaEnchant AQUA_ENCHANT = new AquaEnchant();
	public static final DevourEnchant DEVOUR_ENCHANT = new DevourEnchant();
	public static final DashEnchant DASH_ENCHANT = new DashEnchant();
	public static final HopEnchant HOP_ENCHANT = new HopEnchant();
	public static final RejuvenateEnchant REJUVENATE_ENCHANT = new RejuvenateEnchant();
	public static final BloomEnchant BLOOM_ENCHANT = new BloomEnchant();
	public static final IlluminateEnchant ILLUMINATE_ENCHANT = new IlluminateEnchant();
	public static final WomboComboEnchant WOMBOCOMBO_ENCHANT = new WomboComboEnchant();
	public static final BeastEnchant BEAST_ENCHANT = new BeastEnchant();
	public static final QueasyEnchant QUEASY_ENCHANT = new QueasyEnchant();
	public static final VanishEnchant VANISH_ENCHANT = new VanishEnchant();
	
	// Tools
	public static final SwiftEnchant SWIFT_ENCHANT = new SwiftEnchant();
	public static final BurstEnchant BURST_ENCHANT = new BurstEnchant();
	public static final ObsidianDestroyerEnchant OBSIDIAN_DESTROYER_ENCHANT = new ObsidianDestroyerEnchant();
	
	// Sword
	public static final DischargeEnchant DISCHARGE_ENCHANT = new DischargeEnchant();
	public static final WitheredEnchant WITHERED_ENCHANT = new WitheredEnchant();
	public static final LifeStealEnchant LIFE_STEAL_ENCHANT = new LifeStealEnchant();
	public static final DisappearEnchant DISAPPEAR_ENCHANT = new DisappearEnchant();
	public static final SickenEnchant SICKEN_ENCHANT = new SickenEnchant();
	public static final GrindEnchant GRIND_ENCHANT = new GrindEnchant();
	
	// Bow
	public static final HeadshotEnchant HEADSHOT_ENCHANT = new HeadshotEnchant();
	public static final ChainedEnchant CHAINED_ENCHANT = new ChainedEnchant();
	
	// Axe
	public static final HeavyHitsEnchant HEAVY_HITS_ENCHANT = new HeavyHitsEnchant();
	public static final SlugEnchant SLUG_ENCHANT = new SlugEnchant();
	
}