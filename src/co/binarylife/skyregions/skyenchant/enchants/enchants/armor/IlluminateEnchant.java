package co.binarylife.skyregions.skyenchant.enchants.enchants.armor;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ArmorEnchant;

public class IlluminateEnchant extends ArmorEnchant {

	public IlluminateEnchant() {
		super("Illuminate", 1, 0.75);
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.HELMET;
	}

	@Override
	public void applyEffect(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0));
	}

	@Override
	public void removeEffect(Player p) {
		p.removePotionEffect(PotionEffectType.NIGHT_VISION);
	}

	@Override
	public boolean hasEffect(Player p) {
		return p.hasPotionEffect(PotionEffectType.NIGHT_VISION);
	}

}
