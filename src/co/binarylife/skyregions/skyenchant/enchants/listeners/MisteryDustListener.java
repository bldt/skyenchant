package co.binarylife.skyregions.skyenchant.enchants.listeners;

import co.binarylife.skyregions.skyenchant.Config;
import co.binarylife.skyregions.skyenchant.SkyEnchant;
import co.binarylife.skyregions.skyenchant.enchants.modifiers.Modifier;
import co.binarylife.skyregions.skyenchant.gui.TinkerGUI;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

/**
 * Created by Sneling on 20/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class MisteryDustListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        if(e.getItem() != null && e.getItem().isSimilar(TinkerGUI.misteryDust(1))){
            ItemStack item = e.getItem();

            item.setAmount(item.getAmount() - 1);

            double r1 = new Random().nextDouble(), r2 = randomChance();

            if(r1 < r2){
                e.getPlayer().getInventory().addItem(Modifier.ANGEL_DUST.toItemStack(1));
            }

            e.getPlayer().setItemInHand(item);
            Bukkit.getScheduler().runTaskLaterAsynchronously(SkyEnchant.getInstance(), () -> e.getPlayer().updateInventory(), 3);
        }
    }

    private double randomChance(){
        double df = Config.MISTERY_HI - Config.MISTERY_LO;

        return Config.MISTERY_LO + df * new Random().nextDouble();
    }

}