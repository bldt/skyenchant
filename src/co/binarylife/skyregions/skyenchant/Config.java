package co.binarylife.skyregions.skyenchant;

import net.sneling.binarylife.skyregions.api.files.SkyYMLClassConfig;

import java.io.File;
import java.io.IOException;

/**
 * Created by Sneling on 27/11/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Config extends SkyYMLClassConfig {

    public static int PRICE_T1 = 40;
    public static int PRICE_T2 = 50;
    public static int PRICE_T3 = 60;

    public static double MISTERY_LO = .01;
    public static double MISTERY_HI = .05;

    public static double ANGEL_INCREASE = .02;
    public static double BLACKSCROLL_INCREASE = .05;

    public Config() {
        super(SkyEnchant.getInstance(), new File(SkyEnchant.getInstance().getDataFolder() + "/config.yml"));
    }

    public double getDouble(String path, double def){
        if(!getConfig().isSet(path)){
            getConfig().set(path, def);

            try {
                save();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return def;
        }

        return getConfig().getDouble(path);
    }

}