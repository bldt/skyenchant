package co.binarylife.skyregions.skyenchant;

import co.binarylife.skyregions.skyenchant.commands.SkyEnchantCommand;
import co.binarylife.skyregions.skyenchant.enchants.listeners.ApplicationListener;
import co.binarylife.skyregions.skyenchant.enchants.listeners.ArmorListener;
import co.binarylife.skyregions.skyenchant.enchants.listeners.MisteryDustListener;
import co.binarylife.skyregions.skyenchant.enchants.listeners.PlayerListener;
import co.binarylife.skyregions.skyenchant.enchants.modifiers.Modifier;
import co.binarylife.skyregions.skyenchant.lang.Lang;
import co.binarylife.skyregions.skyenchant.perm.Perm;
import co.binarylife.skyregions.skyenchant.util.EnchantUtil;
import net.sneling.binarylife.skyregions.api.SkyAPI;
import net.sneling.binarylife.skyregions.api.permissions.SkyPermission;
import net.sneling.binarylife.skyregions.api.plugin.SkyPlugin;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

/**
 * Created by Sneling on 18/11/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class SkyEnchant extends SkyPlugin {

    private static SkyEnchant instance;

    private Config config;
    public static boolean OVERRIDE = false; // TODO: 20/12/2016 TEST VAULT

    public SkyEnchant() {
        instance = this;
    }
    
    // Pre load
    public void onLoad(){
        try {
            new Lang();
            config = new Config();

            SkyPermission.load(Perm.class, "skyenchants");

            Modifier.loadModifiers();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    // Load
    public void onEnable(){
        // TODO: 21/12/2016 GENERATE ALL STRINGS FROM:
        //  - ApplicationResponse
        //  - (Should be done when inventory is created on bootup?)

        SkyAPI.getInstance().getCommandManager().registerCommand(new SkyEnchantCommand(), this);
//        SkyAPI.getInstance().getCommandManager().registerCommand(new TinkerCommand(), this);

        try {
            EnchantUtil.registerEnchants();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        getServer().getPluginManager().registerEvents(new PlayerListener(), this);
        getServer().getPluginManager().registerEvents(new ArmorListener(), this);
        getServer().getPluginManager().registerEvents(new ApplicationListener(), this);
        getServer().getPluginManager().registerEvents(new MisteryDustListener(), this);
    }

    public void onDisable(){

    }

    public void registerListener(Listener listener){
        getServer().getPluginManager().registerEvents(listener, this);
    }
    
    public static SkyEnchant getInstance() {
        return instance;
    }

    public Config getPluginConfig(){
        return config;
    }
    
    public WorldGuardPlugin getWorldGuard() {
    	Plugin wg = getServer().getPluginManager().getPlugin("WorldGuard");
    	
    	if (wg == null || !(wg instanceof WorldGuardPlugin)) {
    		return null; // No WG
    	}
    	
    	return (WorldGuardPlugin) wg;
    }

}