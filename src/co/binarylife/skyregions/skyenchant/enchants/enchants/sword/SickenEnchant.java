package co.binarylife.skyregions.skyenchant.enchants.enchants.sword;


import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

@SuppressWarnings("Duplicates")
public class SickenEnchant extends ToolEnchant {

	public SickenEnchant() {
		super("Sicken", 2, 0.8);
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		
		if (!(event.getDamager() instanceof Player)
				|| !(event.getEntity() instanceof Player)) return;
		
		Player damager = (Player) event.getDamager();
		Player target = (Player) event.getEntity();
		ItemStack sword = damager.getItemInHand();
		
		if (hasEnchant(sword)) {
		
			double chance = getChance(0.1);
			
			if (new Random().nextDouble() <= chance)
				applyEffect(target);
			
		}
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.SWORD;
	}

	@Override
	public void applyEffect(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 4 * 20, 0));
	}

}
