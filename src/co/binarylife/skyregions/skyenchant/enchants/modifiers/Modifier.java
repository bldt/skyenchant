package co.binarylife.skyregions.skyenchant.enchants.modifiers;

import co.binarylife.skyregions.skyenchant.lang.Lang;
import net.sneling.binarylife.skyregions.api.lang.SkyLanguage;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sneling on 21/11/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public abstract class Modifier {

    public static Modifier WHITE_SCROLL = new WhiteScroll();
    public static Modifier ANGEL_DUST = new AngelDust();
    public static Modifier BLACK_SCROLL = new BlackScroll();
    private static List<Modifier> modifiers = new ArrayList<>();

    private String defaultName;

    Modifier(String defaultName){
        this.defaultName = defaultName;
    }

    public abstract ApplicationType applyiesOn();
    
    public abstract ItemStack toItemStack(int tier);

    public abstract boolean matchItem(ItemStack item);

    public abstract int getPrice();

    public String getName(){
        return Lang.valueOf("modifiers." + SkyLanguage.toID(defaultName) + ".name", true, defaultName);
    }

    public List<String> getLore(){
        String s = Lang.valueOf("modifiers." + SkyLanguage.toID(defaultName) + ".lores",
                true, "You can set lores%NL%By changing sentences here%NL%Change line by adding '% NL%'");

        return Arrays.asList(s.split("%NL%"));
    }
    
	public enum ApplicationType {
		ITEMS,
		ENCHANTS
	}

	public static void loadModifiers() throws IllegalAccessException {
        List<Modifier> res = new ArrayList<>();

        for(Field f: Modifier.class.getDeclaredFields()){
            if(f.getType().equals(Modifier.class)){
                res.add((Modifier) f.get(Modifier.class));
            }
        }

        modifiers = res;
    }

	public static List<Modifier> getModifiers(){
	    return modifiers;
    }

}