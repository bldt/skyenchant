package co.binarylife.skyregions.skyenchant.util;

import co.binarylife.skyregions.skyenchant.enchants.SkyEnchants;
import co.binarylife.skyregions.skyenchant.enchants.enchants.CustomEnchant;
import co.binarylife.skyregions.skyenchant.lang.Lang;
import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Sneling on 19/11/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
@SuppressWarnings("WeakerAccess")
public class EnchantUtil {

    private static List<CustomEnchant> registeredEnchants;

    public static boolean hasEnchant(CustomEnchant enchant, ItemStack item){
        if(item == null || !item.hasItemMeta() || !item.getItemMeta().hasLore())
            return false;

        for(String lore: item.getItemMeta().getLore())
            if(lore.contains(enchant.getName()))
                return true;

        return false;
    }
    
    public static List<CustomEnchant> getAppliedEnchants(ItemStack item) {
        List<CustomEnchant> enchants = new ArrayList<>();

        if(item == null || !item.hasItemMeta() || !item.getItemMeta().hasLore())
            return enchants;

        enchants.addAll(getRegisteredEnchants().stream().filter(enchant -> hasEnchant(enchant, item)).collect(Collectors.toList()));

        return enchants;
    }

    public static List<CustomEnchant> registerEnchants() throws IllegalAccessException {
        List<CustomEnchant> enchants = new ArrayList<>();

        for(Field f: SkyEnchants.class.getDeclaredFields()) {
            if (CustomEnchant.class.isAssignableFrom(f.getType())) {
                enchants.add((CustomEnchant) f.get(SkyEnchants.class));
            }
        }

        registeredEnchants = enchants;

        return enchants;
    }

    public static List<CustomEnchant> getRegisteredEnchants() {
        return registeredEnchants;
    }

    public static double getModifiedSuccessRate(ItemStack item){
        double rate = 0;

        if(!item.hasItemMeta() || !item.getItemMeta().hasLore())
            return rate;

        for(String s: item.getItemMeta().getLore())
            if(s.contains(Lang.ENCHANT_LORES_SUCCESS.split("%VAL%")[0])) // TODO: 01/12/2016 UNIT TEST
                return getRate(s);

        return rate;
    }
    
    public static double getModifiedDestroyRate(ItemStack item) {
    	double rate = 0;
    	
    	if (!item.hasItemMeta() || !item.getItemMeta().hasLore())
    		return rate;
    	
    	for (String s : item.getItemMeta().getLore()) {
    		if (s.contains(Lang.ENCHANT_LORES_DESTROY.split("%VAL%")[0]))
    			return getDestroyRate(s);
    	} 
    	
    	return rate;
    }

    public static double getDestroyRate(String s){
        s = ChatColor.stripColor(s);
        String[] split = ChatColor.stripColor(Lang.ENCHANT_LORES_DESTROY).split("%VAL%");

        for(String part: split)
            s = s.replace(part, "");

        return Double.parseDouble(s) / 100;
    }

    public static double getRate(String s){
//        Example:
//        Success Rate: +20%
//        will be transformed into
//        +20
        s = ChatColor.stripColor(s);
        String[] split = ChatColor.stripColor(Lang.ENCHANT_LORES_SUCCESS).split("%VAL%");

        for(String part: split)
            s = s.replace(part, "");

        return Double.parseDouble(s) / 100;
    }

    public static void modifyRateBy(ItemStack item, double val){
        if(getModifiedSuccessRate(item) > 0)
            val += getModifiedSuccessRate(item); // make the value that has to be applied = to val + previous value


        // TODO: 28/11/2016 Do we want to have the success rate line as first (or second if there is a white scroll?), or just keep it at the bottom?

        ItemMeta meta = item.getItemMeta();

        List<String> lores = meta.getLore();

        for(int i = 0; i < lores.size(); i++) {
            if (lores.get(i).contains(Lang.ENCHANT_LORES_SUCCESS.split("%VAL%")[0])) {
                lores.set(i, Lang.ENCHANT_LORES_SUCCESS.replace("%VAL%", "" + val * 100));
                meta.setLore(lores);
                item.setItemMeta(meta);
                return;
            }
        }

        // NO SUCCESS RATE FOUND
        lores.add(Lang.ENCHANT_LORES_SUCCESS.replaceAll("%VAL%", "" + val * 100));
        meta.setLore(lores);
        item.setItemMeta(meta);
    }

    public static void resetModifiedRate(ItemStack itemStack){
        ItemMeta meta = itemStack.getItemMeta();
        List<String> lores = meta.getLore();

        for(String s: lores) {
            if (s.matches(StringEscapeUtils.escapeJava(Lang.ENCHANT_LORES_SUCCESS.replaceAll("%VAL%", "[\\d]*")))) { // TODO: 01/12/2016 UNIT TEST
                lores.remove(s);

                meta.setLore(lores);
                itemStack.setItemMeta(meta);

                // We can assume that the application is done properly and that we only need to check for the first line.
                // We might want to disable the return if the other system rarely glitches.
            }
        }
    }

    public static boolean removeEnchant(ItemStack item, CustomEnchant enchant) {
        if (!hasEnchant(enchant, item)) // also checks for the rest
            return false;

        ItemMeta meta = item.getItemMeta();
        List<String> lores = meta.getLore();

        for (String lore : lores) {
            if (lore.contains(enchant.getName())) {
                lores.remove(lore);
                meta.setLore(lores);
                item.setItemMeta(meta);

                if (getAppliedEnchants(item).size() == 0 && item.getEnchantments().containsKey(Enchantment.LURE)) {
                    item.removeEnchantment(Enchantment.LURE);

                    meta = item.getItemMeta();
                    meta.removeItemFlags(ItemFlag.HIDE_ENCHANTS);

                    item.setItemMeta(meta);
                }

                return true;
            }
        }

        return false;
    }

    public static void giveItemProtection(ItemStack item){
        if(item == null || isItemProtected(item))
            return;

    	ItemMeta meta = item.getItemMeta();
        List<String> lores = meta.getLore();

        if(lores == null)
            lores = new ArrayList<>();

        lores.add(Lang.ITEM_PROTECTED);
        meta.setLore(lores);
        item.setItemMeta(meta);
    }

    public static void removeItemProtection(ItemStack item){
        if(item == null || !isItemProtected(item))
            return;

        ItemMeta meta = item.getItemMeta();
        List<String> lores = meta.getLore(); // we know it has lores as it is protected
        lores.remove(Lang.ITEM_PROTECTED);
        meta.setLore(lores);
        item.setItemMeta(meta);
    }
    
    // TODO
    public static boolean isItemProtected(ItemStack item) {
        return item.hasItemMeta() && item.getItemMeta().hasLore() && item.getItemMeta().getLore().contains(Lang.ITEM_PROTECTED);
    }

    public static boolean isEnchant(ItemStack item){
        return getEnchant(item) != null;
    }

    /**
     * Get the enchant for a book
     * @param item
     * @return
     */
    public static CustomEnchant getEnchant(ItemStack item){
        for(CustomEnchant enchant : getRegisteredEnchants())
            if(enchant.matches(item))
                return enchant;

        return null;
    }

    public static CustomEnchant getEnchant(String name){
        for(CustomEnchant enchant: getRegisteredEnchants())
            if (enchant.getName().replaceAll(" ", "_").equalsIgnoreCase(name))
                return enchant;

        return null;
    }

}