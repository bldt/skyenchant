package co.binarylife.skyregions.skyenchant.enchants.enchants.sword;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class DisappearEnchant extends ToolEnchant {

	public DisappearEnchant() {
		super("Disappear", 2, 0.7);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		
		if (!(event.getDamager() instanceof Player)
				|| !(event.getEntity() instanceof Player) || event.isCancelled()) return;
		
		Player damager = (Player) event.getDamager();
		Player target = (Player) event.getEntity();
		ItemStack sword = damager.getItemInHand();
		
		if (hasEnchant(sword)) {
			
			double chance = getChance(0.1);
			
			if (new Random().nextDouble() <= chance)
				target.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 4 * 20, 0));
			
		}
		
	}

	@Override
	public void applyEffect(Player p) {
		
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.SWORD;
	}

}
