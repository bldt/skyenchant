package co.binarylife.skyregions.skyenchant.gui;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Sneling on 17/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class SlotInfo {

    ItemStack item;
    ClickAction action;

    public SlotInfo(ItemStack item, ClickAction action) {
        this.item = item;
        this.action = action;
    }

    public interface ClickAction{
        void onClick(int slot, ItemStack current, ItemStack cursor, Player p);
    }

    public ItemStack getItem() {
        return item;
    }

    public ClickAction getAction() {
        return action;
    }
}
