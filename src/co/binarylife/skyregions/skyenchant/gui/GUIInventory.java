package co.binarylife.skyregions.skyenchant.gui;

import co.binarylife.skyregions.skyenchant.SkyEnchant;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;

/**
 * Created by Sneling on 17/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public abstract class GUIInventory implements Listener {

    int rows;
    String name;
    Inventory inventory;

    HashMap<Integer, SlotInfo> items = new HashMap<>();

    public GUIInventory(int rows, String name) {
        this.rows = rows;
        this.name = name;

        SkyEnchant.getInstance().registerListener(this);
    }

    public void set(int slot, SlotInfo info){
        items.put(slot, info);

        if(slot >= 54)
            throw new IndexOutOfBoundsException();
    }

    public void build(){
        Inventory inventory = Bukkit.createInventory(null, 9 * rows);

        for(int i: items.keySet()){
            inventory.setItem(i, items.get(i).getItem());
        }

        this.inventory = inventory;
    }

    public Inventory getInventory() {
        return inventory;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onClick(InventoryClickEvent e){
    	
        if(e.getClickedInventory() == null || !e.getClickedInventory().equals(inventory) || !(e.getWhoClicked() instanceof Player))
            return;
        
//        if(e.isShiftClick())
//        {
//        	if(e.getClickedInventory().equals(((Player) e.getWhoClicked()).getInventory())) {
//        		Inventory pInv = e.getClickedInventory();
//        		Inventory inv = e.getInventory();
//        		
//        		if(inv.contains(e.getCurrentItem()))
//        			inv.remove(e.getCurrentItem());
//        		
//        		if(!pInv.contains(e.getCurrentItem()))
//        			pInv.remove(e.getCurrentItem());
//        	}
//        }
        e.setCancelled(true);
        
        if(e.getInventory().equals(inventory) && e.isShiftClick())
        	return;
        
        if(items.containsKey(e.getSlot()) && items.get(e.getSlot()).getItem() != null && items.get(e.getSlot()).getAction() != null)
            items.get(e.getSlot()).getAction().onClick(e.getSlot(), e.getCurrentItem(), e.getCursor(), (Player) e.getWhoClicked());
        
        
    }

}