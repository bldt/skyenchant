package co.binarylife.skyregions.skyenchant.util;

import co.binarylife.skyregions.skyenchant.Config;
import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.CustomEnchant;
import co.binarylife.skyregions.skyenchant.enchants.modifiers.Modifier;
import co.binarylife.skyregions.skyenchant.lang.Lang;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Random;

public class ItemUtil {

	public static boolean canApplyTo(ItemStack apply, ItemStack target) {
		if (apply == null || target == null || !apply.hasItemMeta() || !apply.getItemMeta().hasDisplayName())
			return false;

		//noinspection ConstantIfStatement
		if (EnchantUtil.isEnchant(apply)) {
			CustomEnchant enchant = EnchantUtil.getEnchant(apply);

			assert enchant != null;
			for (EnchantmentCategory c : enchant.getCategories()) {
				switch (c) {
					case ARMOR:
						if (isArmor(target))
							return true;

						continue;
					case AXE:
						if (isAxe(target))
							return true;

						continue;
					case BOOTS:
						if (isBoots(target))
							return true;

						continue;
					case BOW:
						if (isBow(target))
							return true;

						continue;
					case HELMET:
						if (isHelmet(target))
							return true;

						continue;
					case PICKAXE:
						if (isPickaxe(target))
							return true;

						continue;
					case SWORD:
						if (isSword(target))
							return true;

						continue;
					case TOOLS:
						if (isTool(target))
							return true;
					case WEAPONS:
						if (isWeapon(target))
							return true;

//					case ALL:
//						return true;
				}
			}

			return false;
		} else if (ModifierUtil.isModifier(apply)) {
			Modifier modifier = ModifierUtil.getModifier(apply);

			if (EnchantUtil.isEnchant(target)) {
				return modifier.applyiesOn() == Modifier.ApplicationType.ENCHANTS;
			} else { // We know that 'target' is a normal item
				return modifier.applyiesOn() == Modifier.ApplicationType.ITEMS;
			}
		}

		return false;
	}

	public static ApplicationResponse apply(Player p, ItemStack apply, ItemStack target) {
		// TODO: 02/12/2016 detect modifier vs enchant, call method

		if (ModifierUtil.isModifier(apply))
			return applyModifier(p, apply, target);
		else if (EnchantUtil.isEnchant(apply))
			return applyEnchant(p, apply, target);

		return ApplicationResponse.NOT_APPLICATION;
	}

	// TODO: 14/12/2016 Detect any modifications
	public static ApplicationResponse applyModifier(Player p, ItemStack apply, ItemStack target) {
		if (!canApplyTo(apply, target))
			return ApplicationResponse.WRONG_TYPE;

		Modifier m = ModifierUtil.getModifier(apply);

		if (m.equals(Modifier.ANGEL_DUST)) {
			EnchantUtil.modifyRateBy(target, Config.ANGEL_INCREASE);
		} else if (m.equals(Modifier.BLACK_SCROLL)) {
			List<CustomEnchant> e = EnchantUtil.getAppliedEnchants(target);
			if (e.size() == 0)
				return ApplicationResponse.MISSING_ENCHANT;

			CustomEnchant enchant = e.get(new Random().nextInt(e.size()));
			EnchantUtil.removeEnchant(target, enchant);
			EnchantUtil.modifyRateBy(target, Config.BLACKSCROLL_INCREASE);
		} else if (m.equals(Modifier.WHITE_SCROLL)) {
			if (EnchantUtil.isItemProtected(target))
				return ApplicationResponse.ALREADY_HAS;

			EnchantUtil.giveItemProtection(target);
		}

		p.updateInventory();

		return ApplicationResponse.SUCCESS;
	}

	// TODO: 14/12/2016 Henri - I will work on the modifier part of this
	public static ApplicationResponse applyEnchant(Player p, ItemStack apply, ItemStack target) {

		if (!canApplyTo(apply, target))
			return ApplicationResponse.WRONG_TYPE;

		if (EnchantUtil.getAppliedEnchants(target).size() >= PlayerUtil.getMaxEnchants(p))
			return ApplicationResponse.TOO_MANY;

		double successRate = 0;
		double destroyRate = 0;

		CustomEnchant e = EnchantUtil.getEnchant(apply);
		successRate = e.getSuccessRate(apply) + EnchantUtil.getModifiedSuccessRate(target);
		destroyRate = e.getDestroyRate(apply) + EnchantUtil.getModifiedDestroyRate(target);

		if (successRate == 0 || destroyRate == 0)
			return ApplicationResponse.NO_RATE;

		// Need to check this before or else a player
		// will be able to destroy an item/waste an enchantment for no reason
		if (e.hasEnchant(target) && (e.getLevel(apply) <= e.getLevel(target))) {
			return ApplicationResponse.LOWER_LEVEL;
		}

		// We create two instances of Random to get two different numbers

		if (new Random().nextDouble() <= successRate) { // Might wanna make sure what is outputed by this is actually what we want
//		if(true){
			if (!e.apply(apply, target))
				// This will never be reached I think
				return ApplicationResponse.LOWER_LEVEL;

			EnchantUtil.resetModifiedRate(target);
			p.updateInventory(); // redundant?

			return ApplicationResponse.SUCCESS;
		} else {
			// If fail, check for destroy
			if (new Random().nextDouble() <= destroyRate) {
				EnchantUtil.resetModifiedRate(target);
				return ApplicationResponse.DESTROY;
			}
		}

		EnchantUtil.resetModifiedRate(target);
		return ApplicationResponse.FAILURE;
	}

	public static boolean isAxe(ItemStack item) {
		return item.getType().name().contains("AXE")
				&& !item.getType().name().contains("PICKAXE");
	}

	public static boolean isBoots(ItemStack item) {
		return item.getType().name().contains("BOOTS");
	}

	public static boolean isHelmet(ItemStack item) {
		return item.getType().name().contains("HELMET");
	}

	public static boolean isChestplate(ItemStack item) {
		return item.getType().name().contains("CHESTPLATE");
	}

	public static boolean isLeggings(ItemStack item) {
		return item.getType().name().contains("LEGGINGS");
	}

	public static boolean isPickaxe(ItemStack item) {
		return item.getType().name().contains("PICKAXE");
	}

	public static boolean isHoe(ItemStack item) {
		return item.getType().name().contains("HOE");
	}

	public static boolean isSpade(ItemStack item) {
		return item.getType().name().contains("SPADE");
	}

	public static boolean isShears(ItemStack item) {
		return item.getType().name().contains("SHEARS");
	}

	public static boolean isBow(ItemStack item) {
		return item.getType().name().contains("BOW");
	}

	public static boolean isRod(ItemStack item) {
		return item.getType().name().contains("FISHING_ROD");
	}

	public static boolean isSword(ItemStack item) {
		return item.getType().name().contains("SWORD");
	}

	public static boolean isCompass(ItemStack item) {
		return item.getType().name().contains("COMPASS");
	}

	public static boolean isMeleeWeapon(ItemStack item) {
		return ItemUtil.isAxe(item) || ItemUtil.isSword(item);
	}

	public static boolean isRangedWeapon(ItemStack item) {
		return ItemUtil.isRod(item) || ItemUtil.isBow(item);
	}

	public static boolean isWeapon(ItemStack item) {
		return ItemUtil.isMeleeWeapon(item) || ItemUtil.isRangedWeapon(item);
	}

	public static boolean isArmor(ItemStack item) {
		return ItemUtil.isHelmet(item) || ItemUtil.isChestplate(item)
				|| ItemUtil.isLeggings(item) || ItemUtil.isBoots(item);
	}

	public static boolean isTool(ItemStack item) {
		return ItemUtil.isPickaxe(item) || ItemUtil.isAxe(item)
				|| ItemUtil.isHoe(item) || ItemUtil.isSpade(item)
				|| ItemUtil.isShears(item) || ItemUtil.isSword(item);
	}

	public enum ApplicationResponse {
		SUCCESS("Success"),
		TOO_MANY("Too many"),
		WRONG_TYPE("Wrong type"),
		FAILURE("Failure"),
		DESTROY("Destroy"),
		NO_RATE("Error while finding rate"),
		NOT_APPLICATION("Cannot Apply"),
		LOWER_LEVEL("Already have this enchant at a lower level"),
		MISSING_ENCHANT("You need an enchant to apply this"),
		ALREADY_HAS("You already have applied this");

		String d;

		ApplicationResponse(String def) {
			d = def;
		}

		public String getMessage() {
			return Lang.valueOf("apply-error." + Lang.toID(this.toString()), true, d);
		}
	}

}
