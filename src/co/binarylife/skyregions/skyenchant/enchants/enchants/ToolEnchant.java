package co.binarylife.skyregions.skyenchant.enchants.enchants;

import org.bukkit.entity.Player;

public abstract class ToolEnchant extends CustomEnchant {

	public ToolEnchant(String name, int maxLevel, double successRate) {
		super(name, maxLevel, successRate);
	}
	
	public abstract void applyEffect(Player p);

}
