package co.binarylife.skyregions.skyenchant.enchants.modifiers;

import co.binarylife.skyregions.skyenchant.util.ItemMaker;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Sneling on 20/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class BlackScroll extends Modifier {

    BlackScroll() {
        super("Black Scroll");
    }

    @Override
    public ApplicationType applyiesOn() {
        return ApplicationType.ITEMS;
    }

    @Override
    public ItemStack toItemStack(int tier) {
        return new ItemMaker(Material.INK_SACK).setName(getName()).setLores(getLore()).getItem();
    }

    @Override
    public boolean matchItem(ItemStack item) {
        return item.isSimilar(toItemStack(1));
    }

    @Override
    public int getPrice() {
        return 40;
    }

}