package co.binarylife.skyregions.skyenchant.enchants.enchants.tools;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;

public class SwiftEnchant extends ToolEnchant {

	public SwiftEnchant() {
		super("Swift", 3, 0.75);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		if (event.getAction() == Action.LEFT_CLICK_BLOCK && hasEnchant(p.getItemInHand()) && !event.isCancelled())
			applyEffect(p);
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.TOOLS;
	}

	@Override
	public void applyEffect(Player p) {
		int level = getLevel(p.getItemInHand());
		int seconds = 0;
		if (level == 1)
			seconds = 5;
		else if (level == 2)
			seconds = 8;
		else if (level == 3)
			seconds = 10;


		p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, seconds * 20, 1));
	}

}
