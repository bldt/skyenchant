package co.binarylife.skyregions.skyenchant.commands;

import co.binarylife.skyregions.skyenchant.enchants.enchants.CustomEnchant;
import co.binarylife.skyregions.skyenchant.enchants.modifiers.Modifier;
import co.binarylife.skyregions.skyenchant.lang.Lang;
import co.binarylife.skyregions.skyenchant.perm.Perm;
import co.binarylife.skyregions.skyenchant.util.EnchantUtil;
import co.binarylife.skyregions.skyenchant.util.ModifierUtil;
import net.sneling.binarylife.skyregions.api.commands.SkyCommand;
import net.sneling.binarylife.skyregions.api.commands.args.ArgInfo;
import net.sneling.binarylife.skyregions.api.commands.args.ArgRequirement;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Sneling on 16/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class GiveCommand extends SkyCommand {

    public GiveCommand() {
        super("give");

        setDescription(Lang.COMMAND_SKYENCHANT_GIVE_DESC);
        setPermission(Perm.COMMANDS_SKYENCHANTS_GIVE);

        addArgument(new ArgInfo("player", ArgInfo.ArgType.MENDATORY, ArgRequirement.PLAYER));
        addArgument(new ArgInfo("enchant/modifier", ArgInfo.ArgType.MENDATORY));
        addArgument(new ArgInfo("name", ArgInfo.ArgType.MENDATORY));
        addArgument(new ArgInfo("level", ArgInfo.ArgType.OPTIONAL, ArgRequirement.INTEGER).setDefault("1"));
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {
        Player p = Bukkit.getPlayer(args.get(0));

        if (args.get(1).equalsIgnoreCase("enchant")) {
        	
        	CustomEnchant enchant = EnchantUtil.getEnchant(args.get(2).replaceAll("_", " "));

            if(enchant == null){
                sender.sendMessage(Lang.COMMAND_SKYENCHANT_GIVE_ENCHANT_NULL.replaceAll("%VAL%", args.get(2)));
                return;
            }

            p.getInventory().addItem(enchant.toItemStack());
        } else if (args.get(1).equalsIgnoreCase("modifier")) {
            Modifier modifier = ModifierUtil.getModifier(args.get(2));

            if(modifier == null){
                sender.sendMessage(Lang.COMMAND_SKYENCHANT_GIVE_MODIFIER_NULL.replaceAll("%VAL%", args.get(2)));
                return;
            }

            p.getInventory().addItem(modifier.toItemStack(Integer.parseInt(args.get(3))));
        } else {
            sender.sendMessage(ChatColor.RED + "Please use 'enchant' or 'modifier' for the 2nd argument");
        }
    }

}