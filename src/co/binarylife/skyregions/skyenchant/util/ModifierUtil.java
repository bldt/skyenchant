package co.binarylife.skyregions.skyenchant.util;

import co.binarylife.skyregions.skyenchant.enchants.modifiers.Modifier;

import org.bukkit.inventory.ItemStack;

public class ModifierUtil {

	public static boolean isModifier(ItemStack item) {
		return getModifier(item) != null;
	}

	public static Modifier getModifier(ItemStack item){
		if(item == null || !item.hasItemMeta())
			return null;

		for(Modifier m: Modifier.getModifiers())
			if(m.matchItem(item))
				return m;

		return null;
	}

	public static Modifier getModifier(String name){
		for(Modifier m: Modifier.getModifiers())
			if(m.getName().replaceAll(" ", "_").equalsIgnoreCase(name))
				return m;

		return null;
	}

}
