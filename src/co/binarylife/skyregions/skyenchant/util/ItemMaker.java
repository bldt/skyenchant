package co.binarylife.skyregions.skyenchant.util;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * Created by Sneling on 22/11/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class ItemMaker {

    private ItemStack item;

    public ItemMaker(Material type){
        this(new ItemStack(type));
    }

    public ItemMaker(ItemStack item) {
        this.item = item;
    }

    public ItemMaker setName(String name){
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);

        return this;
    }

    public ItemMaker setDurability(short data){
        item.setDurability(data);
        return this;
    }

    public ItemMaker setLores(List<String> lores){
        ItemMeta meta = item.getItemMeta();
        meta.setLore(lores);

        item.setItemMeta(meta);

        return this;
    }

    public ItemMaker addGlow() {
        item.addUnsafeEnchantment(Enchantment.LURE, 1);
        ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        item.setItemMeta(meta);

        return this;
    }

    public ItemMaker setAmount(int s){
        item.setAmount(s);
        return this;
    }

    public ItemStack getItem() {
    	return item;
    }

}