package co.binarylife.skyregions.skyenchant.enchants.enchants.bow;

import java.util.Random;

import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;

public class ChainedEnchant extends ToolEnchant {

	public ChainedEnchant() {
		super("Chained", 2, 0.7);
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		
		if (!(event.getDamager() instanceof Projectile)) return;
		if (!(event.getEntity() instanceof Player)) return;
		Projectile proj = (Projectile) event.getDamager();
		ProjectileSource shooter = proj.getShooter();
		if (!(shooter instanceof  Player)) return;
		Player p = (Player) shooter;
		
		if (hasEnchant(p.getItemInHand())) {
			
			double chance = getChance(0.05);
			
			if (new Random().nextDouble() <= chance)
				applyEffect((Player) event.getEntity());
			
		}
		
	}

	@Override
	public void applyEffect(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 40, 5));
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.BOW;
	}

}
