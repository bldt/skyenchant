package co.binarylife.skyregions.skyenchant.gui.enchants;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.gui.GUIHandler;
import co.binarylife.skyregions.skyenchant.gui.GUIInventory;
import co.binarylife.skyregions.skyenchant.gui.SlotInfo;
import co.binarylife.skyregions.skyenchant.gui.StaticInfos;
import co.binarylife.skyregions.skyenchant.lang.Lang;

/**
 * Created by Sneling on 17/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class MainEnchantPage extends GUIInventory {

    public MainEnchantPage() {
        super(2, Lang.GUI_ENCHANTS_MAIN);

        set(2, new SlotInfo(EnchantmentCategory.ARMOR.toItemStack(), (slot, current, cursor, p) -> GUIHandler.open(p, GUIHandler.ENCHANT_ARMOR)));
        set(3, new SlotInfo(EnchantmentCategory.BOW.toItemStack(), (slot, current, cursor, p) -> GUIHandler.open(p, GUIHandler.ENCHANT_BOW)));
        set(4, new SlotInfo(EnchantmentCategory.SWORD.toItemStack(), (slot, current, cursor, p) -> GUIHandler.open(p, GUIHandler.ENCHANT_SWORD)));
        set(5, new SlotInfo(EnchantmentCategory.AXE.toItemStack(), (slot, current, cursor, p) -> GUIHandler.open(p, GUIHandler.ENCHANT_AXE)));
        set(6, new SlotInfo(EnchantmentCategory.TOOLS.toItemStack(), (slot, current, cursor, p) -> GUIHandler.open(p, GUIHandler.ENCHANT_TOOLS)));

        set(13, StaticInfos.backItem(GUIHandler.MAIN));

        build();
    }

}