package co.binarylife.skyregions.skyenchant.enchants.enchants;

import co.binarylife.skyregions.skyenchant.SkyEnchant;
import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.lang.Lang;
import co.binarylife.skyregions.skyenchant.util.EnchantUtil;
import net.sneling.binarylife.skyregions.api.util.MathUtil;
import net.sneling.binarylife.skyregions.api.util.StringUtil;
import net.sneling.binarylife.skyregions.api.util.logger.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sneling on 19/11/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
@SuppressWarnings("WeakerAccess")
public abstract class CustomEnchant implements Listener {

    private int id;
    private String name;
    private int tier;
    private double successRate;
    private List<EnchantmentCategory> additional;

    public CustomEnchant(String name, int tier, double successRate) {
//        this.id = id;
        this.name = name;
        this.tier = tier;
        this.successRate = successRate;
        SkyEnchant.getInstance().registerListener(this);

        additional = new ArrayList<>();
        // load chances
        getChance(1);
    }

    public void addCategory(EnchantmentCategory c){
        additional.add(c);
    }

    public abstract EnchantmentCategory getCategory();

    public List<EnchantmentCategory> getCategories(){
        if(!additional.contains(getCategory()))
            additional.add(getCategory());

        return additional;
    }

    public boolean isValidCategory(EnchantmentCategory c){
        return c.equals(getCategory()) || additional.contains(c);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return Lang.valueOf("enchants." + name + ".name", true, name);
    }

    public int getTier() {
        return tier;
    }

    public String formatDisplayName(){
    	if (tier == 1) {
    		return Lang.ENCHANT_NAME_TIER1_COLOR + getName();
    	} else if (tier == 2) {
    		return Lang.ENCHANT_NAME_TIER2_COLOR + getName();
    	} else if (tier == 3) {
    		return Lang.ENCHANT_NAME_TIER3_COLOR + getName();
    	}
    	
        return Lang.ENCHANT_NAME_DEFAULT_COLOR + getName();
    }

    public List<String> getDescription(){
        return Arrays.asList(Lang.valueOf("enchants." + name + ".description", true).split("%NL%"));
    }

    public ItemStack toItemStack(){
        ItemStack item = new ItemStack(Material.ENCHANTED_BOOK);

        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(formatDisplayName());

        meta.setLore(getLores());
        item.setItemMeta(meta);

        return item;
    }

    // Returns the lore of the actual enchatment book item
    public List<String> getLores(){
        List<String> lores = new ArrayList<>();
        lores.add("");
        lores.add(Lang.ENCHANT_LORES_TARGET
                .replaceAll("%VAL%", StringUtil.capitalizeFirst(getCategory().toString().toLowerCase())));

        for(int i = 0; i < getDescription().size(); i++) {
            if (i == 0)
                lores.add(Lang.ENCHANT_LORES_DESCRIPTION
                        .replaceAll("%VAL%", getDescription().get(i)));
            else
                lores.add(ChatColor.WHITE + getDescription().get(i));
        }

        lores.add("");
        lores.add(Lang.ENCHANT_LORES_SUCCESS.replaceAll("%VAL%", MathUtil.formatDecimals(getSuccessRate(null) * 100, 2)));
        lores.add(Lang.ENCHANT_LORES_DESTROY.replaceAll("%VAL%", MathUtil.formatDecimals((1 - getSuccessRate(null)) * 100, 2)));
        lores.add("");
        lores.add(Lang.ENCHANT_LORES_INFO);

        return lores;
    }

    public boolean matches(ItemStack item){
        return item.hasItemMeta() && item.getItemMeta().hasLore() && loreMatches(item.getItemMeta().getLore(), getLores())
        		&& ChatColor.stripColor(item.getItemMeta().getDisplayName()).contains(getName());
    }

    /**
     * This is based on the description of the enchants
     * @param list1
     * @param list2
     * @return
     */
    public boolean loreMatches(List<String> list1, List<String> list2){
        int c1 = -1, c2 = -1;

//        Logger.print(list1.size());

        for(int i = 0; i < list1.size(); i++){ // finding which line is the description on
//            Logger.print(i);

            if(list1.get(i).contains(Lang.ENCHANT_LORES_DESCRIPTION.split("%VAL%")[0]))
                c1 = i;

            

//            Logger.print(c1 + "//" + c2);

//            if(c1 != -1 && c2 != -1) {
//                Logger.print("out");
//                break;
//            }
        }
        
        for(int i = 0; i < list2.size(); i++) {
        	
        	if(list2.get(i).contains(Lang.ENCHANT_LORES_DESCRIPTION.split("%VAL%")[0]))
                c2 = i;
        }

        if(c1 == -1 || c2 == -1)
            return false;

        // comparing the desc
        for(int i = 0; i < list1.size(); i++){
            if(list1.get(i + c1).contains(Lang.ENCHANT_LORES_DESCRIPTION.split("%VAL%")[0]) || list2.get(i + c1).contains(Lang.ENCHANT_LORES_DESCRIPTION.split("%VAL%")[0]))
                return true;

            if(!list1.get(i + c1).equals(list2.get(i + c2)))
                return false;
        }

        return true;
    }

    public int getLevel(ItemStack item){
        if(!EnchantUtil.hasEnchant(this, item))
            return 0;

        if(!item.hasItemMeta() || !item.getItemMeta().hasLore())
            return 0;

        for(String lore : item.getItemMeta().getLore())
            if(lore.contains(getName()))
                return getLevel(lore);

        return 0;
    }

    public int getLevel(String i){
        if(i.matches("(.+\\d)")) { // has number
        	String name = ChatColor.stripColor(i);
            return Integer.parseInt(name.split(" ")[name.split(" ").length - 1]);

//            for(String p: name.split("\\d*"))
//                name = name.replaceFirst(p, "");
//
//            return Integer.parseInt(name);

            // return Integer.parseInt(name.replaceAll(getName() + " ", ""));
        }

        return 1;
    }

    /**
     *
     * @param source The enchant item
     * @param target The item the enchant will be applied on
     * @return
     */
    public boolean apply(ItemStack source, ItemStack target){
    	
    	if (!target.hasItemMeta())
    		target.setItemMeta(Bukkit.getItemFactory().getItemMeta(target.getType()));

    	ItemMeta itemMeta = target.getItemMeta();

        if(target.getEnchantments().size() == 0 && !target.getEnchantments().containsKey(Enchantment.LURE)){
            Logger.print(1);

            target.addUnsafeEnchantment(Enchantment.LURE, 1);

            itemMeta = target.getItemMeta();
            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);

            target.setItemMeta(itemMeta);
        }

    	List<String> lore;
    	if (hasEnchant(target)) {
    		if (getLevel(target) >= getLevel(source.getItemMeta().getDisplayName()))
    		    return false;

    		lore = itemMeta.getLore();
    		lore.remove(lore.indexOf(formatDisplayName()));
    		itemMeta.setLore(lore);
    		
    	}

    	lore = itemMeta.getLore();

        if(lore == null)
            lore = new ArrayList<>();

    	lore.add(formatDisplayName());
    	itemMeta.setLore(lore);
    	target.setItemMeta(itemMeta);

    	return true;
    }
    
    public boolean hasEnchant(ItemStack item){
        return EnchantUtil.hasEnchant(this, item);
    }

    public double getSuccessRate(ItemStack i) {
        if(i == null)
            return getRawSuccess();

        return getRawSuccess() + EnchantUtil.getModifiedSuccessRate(i);
    }
    
    public double getDestroyRate(ItemStack i) {
    	if (i == null)
    		return getRawDestroy();
    	
    	return getRawDestroy() + EnchantUtil.getModifiedDestroyRate(i);
    }

    private double getRawSuccess(){
        return SkyEnchant.getInstance().getPluginConfig().getDouble("enchant." + Lang.toID(name) + ".success", successRate);
    }

    private double getRawDestroy(){
        return SkyEnchant.getInstance().getPluginConfig().getDouble("enchant." + Lang.toID(name) + ".destroy", 1 - getRawSuccess());
    }

    @Deprecated
    protected double getChance(int lvl, double d){
        return getChance(d);
    }

    protected double getChance(double d){
        return SkyEnchant.getInstance().getPluginConfig().getDouble("enchant-chance." + Lang.toID(name), d);
    }

}