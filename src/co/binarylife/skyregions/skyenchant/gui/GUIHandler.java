package co.binarylife.skyregions.skyenchant.gui;

import co.binarylife.skyregions.skyenchant.gui.enchants.*;
import org.bukkit.entity.Player;

/**
 * Created by Sneling on 17/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class GUIHandler {

    public static final GUIInventory
            MAIN = new MainGUI(),
            ENCHANT_MAIN = new MainEnchantPage(),
            ENCHANT_ARMOR = new ArmorPage(),
            ENCHANT_TOOLS = new ToolsPage(),
            ENCHANT_SWORD = new SwordPage(),
            ENCHANT_BOW = new BowPage(),
            ENCHANT_AXE = new AxePage();

    public static void open(Player p, GUIInventory inv) {
        p.openInventory(inv.getInventory());
    }

}