package co.binarylife.skyregions.skyenchant.util;

import co.binarylife.skyregions.skyenchant.Config;
import co.binarylife.skyregions.skyenchant.SkyEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.CustomEnchant;
import co.binarylife.skyregions.skyenchant.lang.Lang;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Sneling on 20/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class BuyUtil {

    public static void buy(Player p, int tier) {
        if (!p.hasPermission("skyenchant.buy." + tier)) {
            p.sendMessage(Lang.BUY_NO_PERM);
            return;
        }

        int price = Config.PRICE_T1;

        if (tier == 2)
            price = Config.PRICE_T2;
        else if (tier == 3)
            price = Config.PRICE_T3;

        if (canWithdraw(p, price)) {
            p.setLevel(p.getLevel() - price);
            CustomEnchant enchant = selectRandom(tier);

            p.getInventory().addItem(enchant.toItemStack());

            p.sendMessage(Lang.BUY_SUCCESS.replaceAll("%TIER%", String.valueOf(tier)));
        } else {
            p.sendMessage(Lang.BUY_NO_MONEY);
        }
    }

    public static boolean canWithdraw(Player p, double amount) {
        return SkyEnchant.OVERRIDE || p.getLevel() >= amount;
    }

    private static CustomEnchant selectRandom(int tier){
        List<CustomEnchant> fit = EnchantUtil.getRegisteredEnchants().stream().filter(ench -> ench.getTier() == tier).collect(Collectors.toList()); // get enchants with a tier equal or > than the one needed

        return fit.get(new Random().nextInt(fit.size())); // return a random one from the list
    }

}