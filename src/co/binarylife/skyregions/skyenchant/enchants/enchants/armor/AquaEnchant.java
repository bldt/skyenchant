package co.binarylife.skyregions.skyenchant.enchants.enchants.armor;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ArmorEnchant;

public class AquaEnchant extends ArmorEnchant {

	public AquaEnchant() {
		super("Aqua", 1, 0.7);
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.HELMET;
	}
	
	@Override
	public void applyEffect(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, Integer.MAX_VALUE, 0));
	}
	
	@Override
	public void removeEffect(Player p) {
		p.removePotionEffect(PotionEffectType.WATER_BREATHING);
	}
	
	@Override
	public boolean hasEffect(Player p) {
		return p.hasPotionEffect(PotionEffectType.WATER_BREATHING);
	}

}
