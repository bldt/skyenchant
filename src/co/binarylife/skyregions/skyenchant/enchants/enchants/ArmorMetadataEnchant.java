package co.binarylife.skyregions.skyenchant.enchants.enchants;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;

import co.binarylife.skyregions.skyenchant.SkyEnchant;

public abstract class ArmorMetadataEnchant extends ArmorEnchant {

	public ArmorMetadataEnchant(String name, int maxLevel, double successRate) {
		super(name, maxLevel, successRate);
	}

	@Override
	public void applyEffect(Player p) {
		p.setMetadata(getName(), new FixedMetadataValue(SkyEnchant.getInstance(), 1));
	}

	@Override
	public void removeEffect(Player p) {
		p.removeMetadata(getName(), SkyEnchant.getInstance());
	}

	@Override
	public boolean hasEffect(Player p) {
		try {
			p.getMetadata(getName()).get(0).asInt();
			return true;
		} catch (Exception e) {}
		return false;
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		if (hasEffect(event.getPlayer())) 
			event.getPlayer().removeMetadata(getName(), SkyEnchant.getInstance());
	} 

}
