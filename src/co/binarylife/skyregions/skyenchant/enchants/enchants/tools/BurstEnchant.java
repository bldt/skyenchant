package co.binarylife.skyregions.skyenchant.enchants.enchants.tools;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;
import co.binarylife.skyregions.skyenchant.util.PlayerUtil;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.util.BlockIterator;

public class BurstEnchant extends ToolEnchant {

	public BurstEnchant() {
		super("Burst", 1, 0.7);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockBreak(BlockBreakEvent event) {
		if (hasEnchant(event.getPlayer().getItemInHand()) && !event.isCancelled() && event.getBlock().getType() != Material.BEDROCK) {
			Player p = event.getPlayer();

			BlockIterator bit = new BlockIterator(p, 10);
			int count = 0;
			while (bit.hasNext()) {
				Block next = bit.next();
				if (next.getType() != Material.AIR || next.getType() != Material.BEDROCK) {
					Block block;
					for (int xOffset = -1; xOffset <= 1; xOffset++) {
						for(int yOffset = -1; yOffset <= 1; yOffset++){
							for(int zOffset = -1; zOffset <= 1; zOffset++){
								block = next.getRelative(xOffset, yOffset, zOffset);

								if(block.getType() == Material.BEDROCK)
									return;
								
								if (!PlayerUtil.canBuild(p, block))
									return;

								block.breakNaturally();
								p.getItemInHand().setDurability((short) (p.getItemInHand().getDurability() - 1));
							}
						}
					}
					next.breakNaturally();

					next.getWorld().playEffect(next.getLocation(), Effect.PARTICLE_SMOKE, 4);
					count++;
				}

				p.updateInventory();

				if (count == 1) {
					break;
				}
			}

			p.getWorld().playSound(p.getLocation(), Sound.EXPLODE, 0.1f, 0.1f);
		}
	}

	@Override
	public void applyEffect(Player p) {
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.TOOLS;
	}

}
