package co.binarylife.skyregions.skyenchant.gui;

import co.binarylife.skyregions.skyenchant.lang.Lang;
import co.binarylife.skyregions.skyenchant.util.BuyUtil;
import co.binarylife.skyregions.skyenchant.util.ItemMaker;
import org.bukkit.Material;

import java.util.Arrays;

/**
 * Created by Sneling on 19/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class MainGUI extends GUIInventory {

    public MainGUI() {
        super(6, Lang.GUI_MAIN);

        for (int i = 0; i < 9; i++) {
            set(i, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").setDurability((short) 15).getItem(), null));
            set(i + 45, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").setDurability((short) 15).getItem(), null));
        }

        for (int i = 9; i <= 36; i += 9) {
            set(i, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").setDurability((short) 15).getItem(), null));
            set(i + 8, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").setDurability((short) 15).getItem(), null));

            set(i + 1, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setDurability((short) 10).getItem(), null));
            set(i + 7, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setDurability((short) 10).getItem(), null));
        }

        for (int i = 10; i < 17; i++) {
            set(i, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").setDurability((short) 10).getItem(), null));
            set(i + 27, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").setDurability((short) 10).getItem(), null));
        }

        set(20, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE)
                .setName(Lang.GUI_BUY_T1_NAME).setLores(Arrays.asList(Lang.GUI_BUY_T1_LORE.split("%NL%"))).setDurability((short) 4).addGlow().getItem(),
                (slot, current, cursor, p) -> BuyUtil.buy(p, 1)));
        set(22, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE)
                .setName(Lang.GUI_BUY_T2_NAME).setLores(Arrays.asList(Lang.GUI_BUY_T2_LORE.split("%NL%"))).setDurability((short) 1).addGlow().getItem(),
                (slot, current, cursor, p) -> BuyUtil.buy(p, 2)));
        set(24, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE)
                .setName(Lang.GUI_BUY_T3_NAME).setLores(Arrays.asList(Lang.GUI_BUY_T3_LORE.split("%NL%"))).setDurability((short) 14).addGlow().getItem(),
                (slot, current, cursor, p) -> BuyUtil.buy(p, 3)));

        set(21, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").getItem(), null));
        set(23, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").getItem(), null));
        set(29, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").getItem(), null));
        set(31, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").getItem(), null));
        set(33, new SlotInfo(new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").getItem(), null));

        set(30, new SlotInfo(new ItemMaker(Material.COMPASS).setName(Lang.GUI_INFO_ENCH_NAME).setLores(Arrays.asList(Lang.GUI_INFO_ENCH_LORES.split("%NL%"))).getItem(),
                (slot, current, cursor, p) -> GUIHandler.open(p, GUIHandler.ENCHANT_MAIN)));
        set(32, new SlotInfo(new ItemMaker(Material.EYE_OF_ENDER).setName(Lang.GUI_INFO_TINKER_NAME).setLores(Arrays.asList(Lang.GUI_INFO_TINKER_LORES.split("%NL%"))).getItem(),
                ((slot, current, cursor, p) -> {
//                    p.closeInventory();
//                    new TinkerGUI(p);
                })));

        build();
    }

}