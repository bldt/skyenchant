package co.binarylife.skyregions.skyenchant.gui.enchants;

import co.binarylife.skyregions.skyenchant.enchants.SkyEnchants;
import co.binarylife.skyregions.skyenchant.gui.GUIHandler;
import co.binarylife.skyregions.skyenchant.gui.GUIInventory;
import co.binarylife.skyregions.skyenchant.gui.StaticInfos;
import co.binarylife.skyregions.skyenchant.lang.Lang;

import static co.binarylife.skyregions.skyenchant.gui.enchants.ED.get;

/**
 * Created by Sneling on 18/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class SwordPage extends GUIInventory {


    public SwordPage() {
        super(1, Lang.GUI_ENCHANTS_SWORD);

        set(0, get(SkyEnchants.DISCHARGE_ENCHANT));
        set(1, get(SkyEnchants.WITHERED_ENCHANT));
        set(2, get(SkyEnchants.LIFE_STEAL_ENCHANT));
        
        set(4, get(SkyEnchants.SICKEN_ENCHANT));
//        set(5, get(SkyEnchants.SWIFT_ENCHANT));
        set(5, get(SkyEnchants.GRIND_ENCHANT));

        set(8, StaticInfos.backItem(GUIHandler.ENCHANT_MAIN));

        build();
    }
}