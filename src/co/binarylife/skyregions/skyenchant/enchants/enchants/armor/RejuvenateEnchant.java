package co.binarylife.skyregions.skyenchant.enchants.enchants.armor;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ArmorEnchant;

public class RejuvenateEnchant extends ArmorEnchant {

	public RejuvenateEnchant() {
		super("Rejuvenate", 3, 0.7);
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.ARMOR;
	}

	@Override
	public void applyEffect(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, 1));
	}

	@Override
	public void removeEffect(Player p) {
		p.removePotionEffect(PotionEffectType.REGENERATION);
	}

	@Override
	public boolean hasEffect(Player p) {
		return p.hasPotionEffect(PotionEffectType.REGENERATION);
	}

}
