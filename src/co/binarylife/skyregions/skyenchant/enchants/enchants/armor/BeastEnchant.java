package co.binarylife.skyregions.skyenchant.enchants.enchants.armor;

import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.binarylife.skyregions.skyenchant.SkyEnchant;
import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.StackedArmorMetadataEnchant;

public class BeastEnchant extends StackedArmorMetadataEnchant {

	public BeastEnchant() {
		super("Beast", 1, 0.75);
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.ARMOR;
	}
	
	@Override
	public void applyEffect(Player p) {
		
		int numStacks = 0;
		if (hasEffect(p)) {
			numStacks = p.getMetadata(getName()).get(0).asInt();
			p.removeMetadata(getName(), SkyEnchant.getInstance());
		}
		
		p.setMetadata(getName(), new FixedMetadataValue(SkyEnchant.getInstance(), numStacks + 1));
		p.sendMessage("[" + getName() + "] You now have: " + (numStacks + 1) + " stacks");
		
		if (numStacks + 1 >= 3)
			p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1));
		
	}

	@Override
	public void removeEffect(Player p) {
		int numStacks = 0;
		
		if (hasEffect(p)) {
			numStacks = p.getMetadata(getName()).get(0).asInt();
			p.removeMetadata(getName(), SkyEnchant.getInstance());
		}
		
		if (numStacks > 1)
			p.setMetadata(getName(), new FixedMetadataValue(SkyEnchant.getInstance(), numStacks - 1));
		else
			p.removeMetadata(getName(), SkyEnchant.getInstance());
		
		p.sendMessage("["+ getName() + "] You now have: " + (numStacks - 1) + " stacks");
		
		if (numStacks - 1 < 3)
			p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
		
	}

}
