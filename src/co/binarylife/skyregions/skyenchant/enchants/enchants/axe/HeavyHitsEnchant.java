package co.binarylife.skyregions.skyenchant.enchants.enchants.axe;

import java.util.Random;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;

public class HeavyHitsEnchant extends ToolEnchant {

	public HeavyHitsEnchant() {
		super("Heavy Hits", 3, 0.7);
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		
		if (!(event.getDamager() instanceof Player)) return;
		
		Player damager = (Player) event.getDamager();
		ItemStack axe = damager.getItemInHand();
		
		if (hasEnchant(axe)) {
			
			double chance = getChance(0.05);
			
			if (new Random().nextDouble() <= chance)
				applyEffect(damager);
			
		}
	}

	@Override
	public void applyEffect(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 100, 2));
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.AXE;
	}

}
