package co.binarylife.skyregions.skyenchant.enchants.enchants.armor;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ArmorEnchant;

public class DevourEnchant extends ArmorEnchant {

	public DevourEnchant() {
		super("Devour", 1, 0.8);
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.ARMOR;
	}

	@Override
	public void applyEffect(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, Integer.MAX_VALUE, 0));
	}

	@Override
	public void removeEffect(Player p) {
		p.removePotionEffect(PotionEffectType.SATURATION);
	}
	
	@Override
	public boolean hasEffect(Player p) {
		return p.hasPotionEffect(PotionEffectType.SATURATION);
	}

}
