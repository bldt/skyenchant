package co.binarylife.skyregions.skyenchant.enchants.enchants.sword;

import java.util.Random;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;

public class WitheredEnchant extends ToolEnchant {

	public WitheredEnchant() {
		super("Withered", 3, 0.7);
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		
		if (!(event.getDamager() instanceof Player)
				|| !(event.getEntity() instanceof Player)) return;
		
		Player damager = (Player) event.getDamager();
		Player target = (Player) event.getEntity();
		ItemStack sword = damager.getItemInHand();
		
		if (hasEnchant(sword)) {
			
			double chance = getChance(0.1);
			
			if (new Random().nextDouble() <= chance)
				applyEffect(target);
			
		}
	}

	@Override
	public void applyEffect(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 4 * 20, 0));
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.SWORD;
	}

}
