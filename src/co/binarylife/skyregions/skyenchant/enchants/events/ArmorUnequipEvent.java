package co.binarylife.skyregions.skyenchant.enchants.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class ArmorUnequipEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private Player player;
	private ItemStack item;
	
	
	public ArmorUnequipEvent(Player player, ItemStack item) {
		this.player = player;
		this.item = item;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}

	public Player getPlayer() {
		return player;
	}

	public ItemStack getItem() {
		return item;
	}
	
}
