package co.binarylife.skyregions.skyenchant.commands;

import co.binarylife.skyregions.skyenchant.lang.Lang;
import co.binarylife.skyregions.skyenchant.perm.Perm;
import net.sneling.binarylife.skyregions.api.commands.SkyCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.io.IOException;
import java.util.List;

/**
 * Created by Sneling on 19/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class ReloadCommand extends SkyCommand {

    public ReloadCommand() {
        super("reload");

        setPermission(Perm.COMMANDS_SKYENCHANTS_RELOAD);
        setDescription("Reload the Language");
    }

    @Override
    public void execute(CommandSender sender, List<String> args){
        try {
            Lang.instance.loadResource();
        } catch (IllegalAccessException | IOException e) {
            e.printStackTrace();
        }

        sender.sendMessage(ChatColor.GREEN + "Done.");
    }


}