package co.binarylife.skyregions.skyenchant.util;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permissible;

import co.binarylife.skyregions.skyenchant.SkyEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ArmorEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.CustomEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.StackedArmorMetadataEnchant;

/**
 * Created by Sneling on 02/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class PlayerUtil {

    public static int getMaxEnchants(Permissible p){ // TODO: 02/12/2016
        return 4;
    }
    
    public static boolean hasEmptyContainerSlots(Player p) {
    	
    	for (ItemStack i : p.getInventory().getContents())
    		if (i == null)
    			return true;
    	
    	return false;
    }
    
    public static boolean hasEmptyHotbarSlots(Player p) {
    	
    	for (int i = 0; i < 9; i++)
    		if (p.getInventory().getItem(i) == null)
    			return true;
    	
    	return false;
    }
    
    public static void applyArmorEnchants(Player p) {
    	
    	for (ItemStack i : p.getInventory().getArmorContents()) {
    		for (CustomEnchant e : EnchantUtil.getAppliedEnchants(i)) {
    			if (!(e instanceof ArmorEnchant)) continue;
    			ArmorEnchant enchant = (ArmorEnchant) e;
    			if (enchant.hasEnchant(i)) {
    				if (enchant.hasEffect(p) && !(enchant instanceof StackedArmorMetadataEnchant)) { 
    					continue;
    				}
    				enchant.applyEffect(p);
    			}
    		}
    	}
    }
    
    public static boolean canBuild(Player p, Block b) {
    	return SkyEnchant.getInstance().getWorldGuard().canBuild(p, b);
    }
    
    public static boolean canBuild(Player p, Location l) {
    	return SkyEnchant.getInstance().getWorldGuard().canBuild(p, l);
    }

}