package co.binarylife.skyregions.skyenchant.gui;

import co.binarylife.skyregions.skyenchant.SkyEnchant;
import co.binarylife.skyregions.skyenchant.lang.Lang;
import co.binarylife.skyregions.skyenchant.util.ItemMaker;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sneling on 20/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class TinkerGUI implements Listener {

    private Inventory inv;
    private Player p;

    public TinkerGUI(Player p) {
        inv = Bukkit.createInventory(p, 54, Lang.GUI_TINKERER_NAME);

        this.p = p;

        inv.setItem(0, new ItemMaker(Material.STAINED_GLASS_PANE).setDurability((short) 14).setName(Lang.GUI_TINKERER_TRADE).getItem());
        inv.setItem(8, new ItemMaker(Material.STAINED_GLASS_PANE).setDurability((short) 14).setName(Lang.GUI_TINKERER_TRADE).getItem());

        for(int i = 4; i < 53; i += 9)
            inv.setItem(i, new ItemMaker(Material.STAINED_GLASS_PANE).setName(" ").getItem());

        Bukkit.getServer().getPluginManager().registerEvents(this, SkyEnchant.getInstance());
        p.openInventory(inv);
    }

    @EventHandler
    public void onClick(InventoryClickEvent e){
        if(!e.getWhoClicked().equals(p))
            return;

        e.setCancelled(true);

        if(e.getClickedInventory().equals(inv)) {
            if (e.getSlot() == 0 || e.getSlot() == 8) {
                trade();
                return;
            }

            if (e.getSlot() % 9 < 5 && e.getCurrentItem() != null) {
                removeItem(e.getSlot());
            }
        }else if(e.getClickedInventory().equals(p.getInventory())){
            if(e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.ENCHANTED_BOOK){
                addItem(e.getCurrentItem(), e.getSlot());
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e){
        for(ItemStack item: collect())
            p.getInventory().addItem(item);

        Bukkit.getScheduler().runTaskLaterAsynchronously(SkyEnchant.getInstance(), () -> p.updateInventory(), 3);

        InventoryClickEvent.getHandlerList().unregister(this);
        InventoryCloseEvent.getHandlerList().unregister(this);
    }

    private void trade(){
        List<ItemStack> item = collect();

        inv.remove(Material.ENCHANTED_BOOK);
        if(item.size() > 0)
            p.getInventory().addItem(misteryDust(item.size()));

        p.closeInventory();

        Bukkit.getScheduler().runTaskLaterAsynchronously(SkyEnchant.getInstance(), () -> p.updateInventory(), 3);
    }

    private boolean removeItem(int slot){
        if(inv.getItem(slot) == null || inv.getItem(slot).getType() != Material.ENCHANTED_BOOK)
            return false;

        p.getInventory().addItem(inv.getItem(slot));
        inv.setItem(slot, null);

        if(slot < 9)
            slot += 4;
        else
            slot += 5;

        inv.setItem(slot, null);

        return true;
    }

    private boolean addItem(ItemStack item, int s){
        int slot = getFreeSlot();

        if(slot == -1)
            return false;

        inv.setItem(slot, item);

        if(slot < 9)
            slot += 4;
        else
            slot += 5;

        inv.setItem(slot, misteryDust(1));

        p.getInventory().setItem(s, null);

        Bukkit.getScheduler().runTaskLaterAsynchronously(SkyEnchant.getInstance(), () -> p.updateInventory(), 2);

        return true;
    }

    private int getFreeSlot(){
        for(int i = 1; i < 53; i++) {
            if (i % 9 == 4) {
                i += 4;
                continue;
            }

            if(inv.getItem(i) == null || (inv.getItem(i).getType() != Material.ENCHANTED_BOOK && inv.getItem(i).getType() != Material.STAINED_GLASS_PANE))
                return i;
        }

        return -1;
    }

    private List<ItemStack> collect() {
        List<ItemStack> items = new ArrayList<>();

        for(int i = 1; i < 53; i++){
            if(i % 9 == 4){
                i += 4;
                continue;
            }

            if(inv.getItem(i) != null && inv.getItem(i).getType() == Material.ENCHANTED_BOOK)
                items.add(inv.getItem(i));
        }

        return items;
    }

    public static ItemStack misteryDust(int amount){
        return new ItemMaker(Material.SULPHUR).setAmount(amount).setName(Lang.ITEM_MISTERY_NAME).setLores(Arrays.asList(Lang.ITEM_MISTERY_LORE.split("%NL%"))).getItem();
    }



}