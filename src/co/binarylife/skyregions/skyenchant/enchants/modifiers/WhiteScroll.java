package co.binarylife.skyregions.skyenchant.enchants.modifiers;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Sneling on 27/11/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class WhiteScroll extends Modifier {
	
	public static final ChatColor nameColor = ChatColor.GRAY;

    public WhiteScroll() {
        super("White Scroll");
    }

    @Override
    public ApplicationType applyiesOn() {
        return ApplicationType.ITEMS;
    }

    @Override
    public ItemStack toItemStack(int tier) {
        ItemStack item = new ItemStack(Material.PAPER);

        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getLore());
        item.setItemMeta(meta);

        return item;
    }

    @Override
    public boolean matchItem(ItemStack item) {
        return item.isSimilar(toItemStack(0));
    }

    @Override
    public int getPrice() {
        return 50;
    }

}