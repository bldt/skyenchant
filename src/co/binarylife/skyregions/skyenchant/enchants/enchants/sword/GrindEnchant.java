package co.binarylife.skyregions.skyenchant.enchants.enchants.sword;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.Random;

public class GrindEnchant extends ToolEnchant {

	public GrindEnchant() {
		super("Grind", 3, 0.7);
	}
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		
		if (!(event.getDamager() instanceof Player) || event.isCancelled()) return;
		Player damager = (Player) event.getDamager();
		
		if (((Damageable) event.getEntity()).getHealth() - event.getDamage() <= 0 &&
				hasEnchant(damager.getItemInHand())) {
			// Arbitrary values
			int baseExp = 2;
			int maxExp = 4;
			// Will generate a random int exp value between baseExp and maxExp
			damager.getWorld().spawn(event.getEntity().getLocation(), ExperienceOrb.class).setExperience(
					new Random().nextInt((maxExp - baseExp) + 1) + baseExp);
		}
		
	}

	@Override
	public void applyEffect(Player p) {
		
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.SWORD;
	}

}
