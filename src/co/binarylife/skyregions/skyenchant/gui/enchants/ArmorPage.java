package co.binarylife.skyregions.skyenchant.gui.enchants;

import co.binarylife.skyregions.skyenchant.enchants.SkyEnchants;
import co.binarylife.skyregions.skyenchant.gui.GUIHandler;
import co.binarylife.skyregions.skyenchant.gui.GUIInventory;
import co.binarylife.skyregions.skyenchant.gui.StaticInfos;
import co.binarylife.skyregions.skyenchant.lang.Lang;

import static co.binarylife.skyregions.skyenchant.gui.enchants.ED.get;

/**
 * Created by Sneling on 17/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class ArmorPage extends GUIInventory {

    public ArmorPage() {
        super(2, Lang.GUI_ENCHANTS_ARMOR);

        set(0, get(SkyEnchants.AQUA_ENCHANT));
        set(1, get(SkyEnchants.BEAST_ENCHANT));
        set(2, get(SkyEnchants.BLOOM_ENCHANT));
        set(3, get(SkyEnchants.DASH_ENCHANT));
        set(4, get(SkyEnchants.DEVOUR_ENCHANT));
        set(5, get(SkyEnchants.HOP_ENCHANT));
        set(6, get(SkyEnchants.ILLUMINATE_ENCHANT));
        set(7, get(SkyEnchants.MAGMA_ENCHANT));
        set(8, get(SkyEnchants.QUEASY_ENCHANT));
        set(9, get(SkyEnchants.REJUVENATE_ENCHANT));
        set(10, get(SkyEnchants.VANISH_ENCHANT));
        set(11, get(SkyEnchants.WOMBOCOMBO_ENCHANT));
        set(12, get(SkyEnchants.DISAPPEAR_ENCHANT));

        set(17, StaticInfos.backItem(GUIHandler.ENCHANT_MAIN));

        build();
    }

}