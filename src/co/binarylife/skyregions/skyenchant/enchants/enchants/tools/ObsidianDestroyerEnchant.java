package co.binarylife.skyregions.skyenchant.enchants.enchants.tools;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;
import co.binarylife.skyregions.skyenchant.util.PlayerUtil;

public class ObsidianDestroyerEnchant extends ToolEnchant {
	
	public ObsidianDestroyerEnchant() {
		super("Obsidian Destroyer", 1, 0.7);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (hasEnchant(event.getPlayer().getItemInHand()) && event.getAction() == Action.LEFT_CLICK_BLOCK) {
			if (event.getClickedBlock().getType() == Material.OBSIDIAN  && !event.isCancelled()) {
				
				if (!PlayerUtil.canBuild(event.getPlayer().getPlayer(), event.getClickedBlock()))
					return;
				
				event.getPlayer().getItemInHand().setDurability((short) (event.getPlayer().getItemInHand().getDurability() - 1));
				event.getClickedBlock().breakNaturally();
				event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ANVIL_LAND, 0.2f, 0.2f);
				event.getPlayer().updateInventory();
			}
		}
	}

	@Override
	public void applyEffect(Player p) {
		// meh bad design
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.TOOLS;
	}

}
