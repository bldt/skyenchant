package co.binarylife.skyregions.skyenchant.gui;

import co.binarylife.skyregions.skyenchant.lang.Lang;
import co.binarylife.skyregions.skyenchant.util.ItemMaker;
import org.bukkit.Material;

/**
 * Created by Sneling on 17/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class StaticInfos {

    public static SlotInfo backItem(GUIInventory target){
        return new SlotInfo(
                new ItemMaker(Material.ARROW).setName(Lang.GUI_BACK).getItem(),
                (slot, current, cursor, p) -> GUIHandler.open(p, target));
    }

}