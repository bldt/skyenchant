package co.binarylife.skyregions.skyenchant.perm;

import net.sneling.binarylife.skyregions.api.permissions.SkyPermission;

/**
 * Created by Sneling on 16/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Perm {

    public static SkyPermission

    COMMANDS_SKYENCHANTS,
    COMMANDS_SKYENCHANTS_GIVE,
    COMMANDS_SKYENCHANTS_RELOAD;


}