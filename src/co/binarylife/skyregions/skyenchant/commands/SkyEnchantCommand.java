package co.binarylife.skyregions.skyenchant.commands;

import co.binarylife.skyregions.skyenchant.gui.GUIHandler;
import co.binarylife.skyregions.skyenchant.lang.Lang;
import co.binarylife.skyregions.skyenchant.perm.Perm;
import net.sneling.binarylife.skyregions.api.commands.SkyCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Sneling on 15/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class SkyEnchantCommand extends SkyCommand {

    public SkyEnchantCommand() {
        super("skyenchant");

        setDescription(Lang.COMMAND_SKYENCHANT_DESC);
        setPermission(Perm.COMMANDS_SKYENCHANTS);

        addAlias("eenchant");

        addChilds(new GiveCommand(), new ReloadCommand());
    }

    @Override
    public void execute(CommandSender sender, List<String> args){
        GUIHandler.open((Player) sender, GUIHandler.MAIN);
    }

}