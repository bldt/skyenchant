package co.binarylife.skyregions.skyenchant.gui.enchants;

import co.binarylife.skyregions.skyenchant.enchants.enchants.CustomEnchant;
import co.binarylife.skyregions.skyenchant.gui.SlotInfo;

/**
 * Created by Sneling on 17/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class ED {

    static SlotInfo get(CustomEnchant enchant){
        return new SlotInfo(enchant.toItemStack(), null);
    }

}