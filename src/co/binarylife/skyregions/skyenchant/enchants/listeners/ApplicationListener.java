package co.binarylife.skyregions.skyenchant.enchants.listeners;

import co.binarylife.skyregions.skyenchant.util.ItemUtil;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class ApplicationListener implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInventoryClick(InventoryClickEvent event) {
		if (!(event.getWhoClicked() instanceof Player)) // making sure the guy who clicked is a player
			return;

		if (event.getClick() != ClickType.LEFT
				|| event.getAction() != InventoryAction.SWAP_WITH_CURSOR) // Only allow an enchant if it is a 'swap'
			return;

		Player p = (Player) event.getWhoClicked();

		ItemStack cursorItem = event.getCursor();
		ItemStack currentItem = event.getCurrentItem();

		// Redundant because we checked InventoryAction, but might as well
		if (cursorItem == null || currentItem == null)
			return;

		ItemUtil.ApplicationResponse response = ItemUtil.apply(p, cursorItem, currentItem); 

		if (response == ItemUtil.ApplicationResponse.NOT_APPLICATION)
			return;
		
		event.setCancelled(true);

		p.sendMessage(response.getMessage());

		// TODO: 02/12/2016
		if (response == ItemUtil.ApplicationResponse.SUCCESS) {
			cursorItem.setAmount(cursorItem.getAmount() -1);

			if(cursorItem.getAmount() == 0)
				p.setItemOnCursor(null);
			else
				p.setItemOnCursor(cursorItem);

			p.playSound(p.getLocation(), Sound.ANVIL_USE, 1.0F, 1.0F);
		} else if (response == ItemUtil.ApplicationResponse.FAILURE) {
			p.setItemOnCursor(null);
			p.playSound(p.getLocation(), Sound.FIZZ, 1.0F, 1.0F);
		} else if (response == ItemUtil.ApplicationResponse.DESTROY) {
			p.setItemOnCursor(null);
			event.setCurrentItem(null);
			p.playSound(p.getLocation(), Sound.ITEM_BREAK, 1.0F, 1.0F);
		}
	}

}
