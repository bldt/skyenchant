package co.binarylife.skyregions.skyenchant.enchants.enchants;

import org.bukkit.entity.Player;

public abstract class ArmorEnchant extends CustomEnchant {
	
	public ArmorEnchant(String name, int maxLevel, double successRate) {
		super(name, maxLevel, successRate);
	}

	public abstract void applyEffect(Player p);
	
	public abstract void removeEffect(Player p);
	
	public abstract boolean hasEffect(Player p);

}
