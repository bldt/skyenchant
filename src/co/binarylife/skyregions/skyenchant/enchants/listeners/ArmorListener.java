package co.binarylife.skyregions.skyenchant.enchants.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import co.binarylife.skyregions.skyenchant.SkyEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ArmorEnchant;
import co.binarylife.skyregions.skyenchant.enchants.enchants.CustomEnchant;
import co.binarylife.skyregions.skyenchant.enchants.events.ArmorEquipEvent;
import co.binarylife.skyregions.skyenchant.enchants.events.ArmorUnequipEvent;
import co.binarylife.skyregions.skyenchant.util.EnchantUtil;
import co.binarylife.skyregions.skyenchant.util.ItemUtil;
import co.binarylife.skyregions.skyenchant.util.PlayerUtil;

public class ArmorListener implements Listener {
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		
		if (!(event.getAction().toString().contains("RIGHT_CLICK")) || !event.hasItem()) 
			return;
		
		if (ItemUtil.isArmor(event.getItem()))
			this.callArmorEquipEvent(event.getPlayer(), event.getItem());
		
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		if (!event.getKeepInventory()) {
			Player p = event.getEntity();
			for (ItemStack item : p.getInventory().getArmorContents()) {
				this.callArmorUnequipEvent(p, item);
			}
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		
		ItemStack item = event.getCurrentItem();
		ItemStack cursor = event.getCursor();
		Player p = (Player) event.getWhoClicked();
		
		// Regular placement and pickup
		if (event.getAction().toString().contains("PICKUP") && event.getSlotType() == SlotType.ARMOR) {
			this.callArmorUnequipEvent(p, item);
			return;
		}
		
		// Check for if the item is actually placed or not
		if (event.getAction().toString().contains("PLACE") && event.getSlotType() == SlotType.ARMOR) {
			if ((ItemUtil.isHelmet(cursor) && event.getSlot() != 39)
					|| (ItemUtil.isChestplate(cursor) && event.getSlot() != 38)
					|| (ItemUtil.isLeggings(cursor) && event.getSlot() != 37)
					|| (ItemUtil.isBoots(cursor) && event.getSlot() != 36))
				return;
			
			this.callArmorEquipEvent(p, cursor);
			return;
		}
		
		// Check for shift left click equip/unequip
		if (event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY && event.getClick().toString().contains("SHIFT")
				&& ItemUtil.isArmor(item)) {
			
			// If the player shift clicks a piece of equipped armor and the equipped armor has a place to fill, call unequip
			if (event.getSlotType() == SlotType.ARMOR && (PlayerUtil.hasEmptyContainerSlots(p)
					|| PlayerUtil.hasEmptyHotbarSlots(p))) {
				this.callArmorUnequipEvent(p, item);
				return;
			}
			
			// If the player shift clicks a piece of unequipped armor, and there is a respective armor slot to fill, call equip
			else if (event.getSlotType() == SlotType.CONTAINER || event.getSlotType() == SlotType.QUICKBAR) {
				
				if ((ItemUtil.isHelmet(item) && p.getInventory().getHelmet() != null)
					|| (ItemUtil.isChestplate(item) && p.getInventory().getChestplate() != null)
					|| (ItemUtil.isLeggings(item) && p.getInventory().getChestplate() != null)
					|| (ItemUtil.isBoots(item) && p.getInventory().getBoots() != null))
					return;
				
				this.callArmorEquipEvent(p, item);
				return;
			}
				
		}
		
		// Swap armor - unequip first then equip
		if (event.getAction() == InventoryAction.SWAP_WITH_CURSOR && event.getSlotType() == SlotType.ARMOR) {
			this.callArmorUnequipEvent(p, item);
			this.callArmorEquipEvent(p, cursor);
			return; // Redundant but WHATEVER
		}
		
	}
	
	@EventHandler
	public void onArmorEquip(ArmorEquipEvent event) {
		
		for (CustomEnchant e : EnchantUtil.getAppliedEnchants(event.getItem())) {
			if (!(e instanceof ArmorEnchant)) continue;
			((ArmorEnchant) e).applyEffect(event.getPlayer());
		}
		
	}
	
	@EventHandler
	public void onArmorUnequip(ArmorUnequipEvent event) {
		for (CustomEnchant e : EnchantUtil.getAppliedEnchants(event.getItem()))  {
			if (!(e instanceof ArmorEnchant)) continue;
			((ArmorEnchant) e).removeEffect(event.getPlayer());
		}
	}
	
	// InventoryMoveItemEvent?
	
	private void callArmorEquipEvent(Player p, ItemStack item) {
		ArmorEquipEvent newEvent = new ArmorEquipEvent(p, item);
		SkyEnchant.getInstance().getServer().getPluginManager().callEvent(newEvent);
	}
	
	private void callArmorUnequipEvent(Player p, ItemStack item) {
		ArmorUnequipEvent newEvent = new ArmorUnequipEvent(p, item);
		SkyEnchant.getInstance().getServer().getPluginManager().callEvent(newEvent);
	}

}
