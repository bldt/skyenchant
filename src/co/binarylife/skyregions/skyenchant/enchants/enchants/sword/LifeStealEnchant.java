package co.binarylife.skyregions.skyenchant.enchants.enchants.sword;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.Random;

public class LifeStealEnchant extends ToolEnchant {

	public LifeStealEnchant() {
		super("Life Steal", 2, 0.7);
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (!(event.getDamager() instanceof Player) || event.isCancelled()) return;
		Player p = (Player) event.getDamager();
		
		if (hasEnchant(p.getItemInHand())) {
			
			double chance = getChance(0.1);
			
			if (new Random().nextDouble() <= chance)
				applyEffect(p);
			
		}
		
	}

	@Override
	public void applyEffect(Player p) {
		if(p.getHealth() + 4 > p.getMaxHealth()) {
			p.setHealth(p.getHealth() + (p.getMaxHealth() - p.getHealth()));
			return;
		}
		p.setHealth(p.getHealth() + 4);
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.SWORD;
	}
	
	

}
