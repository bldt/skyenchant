package co.binarylife.skyregions.skyenchant.enchants.enchants.armor;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ArmorEnchant;

public class DashEnchant extends ArmorEnchant {

	public DashEnchant() {
		super("Dash", 3, 0.7);
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.HELMET;
	}

	@Override
	public void applyEffect(Player p) {
		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
	}

	@Override
	public void removeEffect(Player p) {
		p.removePotionEffect(PotionEffectType.SPEED);
	}

	@Override
	public boolean hasEffect(Player p) {
		return p.hasPotionEffect(PotionEffectType.SPEED);
	}

}
