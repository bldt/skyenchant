package co.binarylife.skyregions.skyenchant.gui.enchants;

import co.binarylife.skyregions.skyenchant.enchants.SkyEnchants;
import co.binarylife.skyregions.skyenchant.gui.GUIHandler;
import co.binarylife.skyregions.skyenchant.gui.GUIInventory;
import co.binarylife.skyregions.skyenchant.gui.StaticInfos;
import co.binarylife.skyregions.skyenchant.lang.Lang;

import static co.binarylife.skyregions.skyenchant.gui.enchants.ED.get;

/**
 * Created by Sneling on 19/12/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class BowPage extends GUIInventory {

    public BowPage() {
        super(1, Lang.GUI_ENCHANTS_TOOLS);

        set(0, get(SkyEnchants.HEADSHOT_ENCHANT));
        set(1, get(SkyEnchants.CHAINED_ENCHANT));

        set(8, StaticInfos.backItem(GUIHandler.ENCHANT_MAIN));

        build();
    }


}