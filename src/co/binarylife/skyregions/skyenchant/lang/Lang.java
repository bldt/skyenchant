package co.binarylife.skyregions.skyenchant.lang;

import co.binarylife.skyregions.skyenchant.SkyEnchant;
import net.sneling.binarylife.skyregions.api.lang.SkyLanguage;
import org.bukkit.ChatColor;

import java.io.IOException;

/**
 * Created by Sneling on 19/11/2016 for SkyEnchant.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Lang extends SkyLanguage {

    public static Lang instance;

    public static String
            DEF = "",
//      Enchants

    ENCHANT_NAME_DEFAULT_COLOR = "&a",
            ENCHANT_NAME_TIER1_COLOR = "&a",
            ENCHANT_NAME_TIER2_COLOR = "&b",
            ENCHANT_NAME_TIER3_COLOR = "&c",

    ENCHANT_LORES_TARGET = "&a* Enchant Target: %VAL%",
            ENCHANT_LORES_DESCRIPTION = "&a* Description: %VAL%",
            ENCHANT_LORES_SUCCESS = "&a* Success Rate: %VAL%",
            ENCHANT_LORES_DESTROY = "&c* Destroy Rate: %VAL%",
            ENCHANT_LORES_INFO = "&3Drag n' Click onto item to enchant.",


    COMMAND_SKYENCHANT_DESC = "SkyEnchant's main command",
            COMMAND_SKYENCHANT_GIVE_DESC = "Give a Enchant/Modifier to a player",
            COMMAND_SKYENCHANT_GIVE_ENCHANT_NULL = "&cEnchant %VAL% not found",
            COMMAND_SKYENCHANT_GIVE_MODIFIER_NULL = "&cModifier %VAL% not found",

    GUI_MAIN = "Main GUI",
            GUI_ENCHANTS_MAIN = "&cEnchantment Info",
            GUI_ENCHANTS_ARMOR = "&cEnchantment Info",
            GUI_ENCHANTS_TOOLS = "&cEnchantment Info",
            GUI_ENCHANTS_SWORD = "&cEnchantment Info",

    ITEM_MISTERY_NAME = "Mistery Dust",
            ITEM_MISTERY_LORE = "Lores%NL%Line",
            ITEM_PROTECTED = "&aThis item is protected",

    GUI_INFO_ENCH_NAME = "Enchants",
            GUI_INFO_ENCH_LORES = "Lores",
            GUI_INFO_TINKER_NAME = "Goto Tinkerer",
            GUI_INFO_TINKER_LORES = "Click me!",

    GUI_BACK = "&aGo Back",
            GUI_BUY_T1_NAME = "Buy a Tier 1 Enchant",
            GUI_BUY_T1_LORE = "Tier 1 Lore%NL%Another line",
            GUI_BUY_T2_NAME = "Buy a Tier 2 Enchant",
            GUI_BUY_T2_LORE = "Tier 2 Lore%NL%Another line",
            GUI_BUY_T3_NAME = "Buy a Tier 3 Enchant",
            GUI_BUY_T3_LORE = "Tier 3 Lore%NL%Another line",
            BUY_NO_PERM = "&cYou cannot buy this tier!",
            BUY_NO_MONEY = "&cYou do not have enough money",
            BUY_SUCCESS = "&aYou bought a Tier %TIER% enchant",
            GUI_TINKERER_NAME = "&cTinkerer",
            GUI_TINKERER_TRADE = "&aTrade those items";

    public Lang() {
        super(SkyEnchant.getInstance(), "lang.yml");

        instance = this;
    }

    public static String valueOf(String name) {
        return valueOf(name, false);
    }

    public static String valueOf(String name, boolean write) {
        return valueOf(name, write, "[NOT SET]");
    }

    public static String valueOf(String name, boolean write, String def) {
        try {
//            Lang.class.getField(name);

            return (String) Lang.class.getDeclaredField(name).get(Lang.class);
        } catch (IllegalAccessException e) {
            e.printStackTrace();

            return "error: access";
        } catch (NoSuchFieldException e) {
//            e.printStackTrace();

            String value = instance.getConfig().getString(toID(name));

            if (value == null) {
                if (write) {
                    instance.getConfig().set(toID(name), def);

                    try {
                        instance.save();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                    return ChatColor.translateAlternateColorCodes('&', def);
                } else {
                    return ChatColor.translateAlternateColorCodes('&', def);
                }
            } else {
                return ChatColor.translateAlternateColorCodes('&', value);
            }
        }
    }

}