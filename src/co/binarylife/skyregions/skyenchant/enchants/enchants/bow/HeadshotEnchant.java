package co.binarylife.skyregions.skyenchant.enchants.enchants.bow;

import java.util.Random;

import org.bukkit.Sound;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.projectiles.ProjectileSource;

import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.ToolEnchant;

public class HeadshotEnchant extends ToolEnchant {

	public HeadshotEnchant() {
		super("Headshot", 3, 0.7);
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		
		if (!(event.getDamager() instanceof Projectile)) return;
		Projectile proj = (Projectile) event.getDamager();
		ProjectileSource shooter = proj.getShooter();
		if (!(shooter instanceof  Player)) return;
		Player p = (Player) shooter;
		
		if (hasEnchant(p.getItemInHand())) {
			
			double chance = getChance(0.05);
			
			// Headshot does extra damage from 2 to 4
			if (new Random().nextDouble() <= chance) {
				((Damageable) event.getEntity()).damage(new Random().nextInt(3) + 2);
				p.playSound(p.getLocation(), Sound.ANVIL_LAND, 0.1f, 0.1f);
			}
			
		}
	}

	@Override
	public void applyEffect(Player p) {
		
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.BOW;
	}

}
