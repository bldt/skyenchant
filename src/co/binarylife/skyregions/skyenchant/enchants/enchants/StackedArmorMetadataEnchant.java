package co.binarylife.skyregions.skyenchant.enchants.enchants;

import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import co.binarylife.skyregions.skyenchant.SkyEnchant;

public abstract class StackedArmorMetadataEnchant extends ArmorMetadataEnchant {

	public StackedArmorMetadataEnchant(String name, int maxLevel, double successRate) {
		super(name, maxLevel, successRate);
	}
	
	@Override
	public void applyEffect(Player p) {
		int numStacks = 0;
		if (hasEffect(p)) {
			numStacks = p.getMetadata(getName()).get(0).asInt();
			p.removeMetadata(getName(), SkyEnchant.getInstance());
		}
		p.setMetadata(getName(), new FixedMetadataValue(SkyEnchant.getInstance(), numStacks + 1));
		p.sendMessage("[" + getName() + "] You now have: " + (numStacks + 1) + " stacks");
	}

	@Override
	public void removeEffect(Player p) {
		int numStacks = 0;
		
		if (hasEffect(p)) {
			numStacks = p.getMetadata(getName()).get(0).asInt();
			p.removeMetadata(getName(), SkyEnchant.getInstance());
		}
		
		if (numStacks > 1)
			p.setMetadata(getName(), new FixedMetadataValue(SkyEnchant.getInstance(), numStacks - 1));
		else
			p.removeMetadata(getName(), SkyEnchant.getInstance());
		p.sendMessage("["+ getName() + "] You now have: " + (numStacks - 1) + " stacks");
		
	}

}
