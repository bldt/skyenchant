package co.binarylife.skyregions.skyenchant.enchants.enchants.armor;

import java.util.Random;

import org.bukkit.Effect;
import org.bukkit.EntityEffect;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import co.binarylife.skyregions.skyenchant.SkyEnchant;
import co.binarylife.skyregions.skyenchant.enchants.EnchantmentCategory;
import co.binarylife.skyregions.skyenchant.enchants.enchants.StackedArmorMetadataEnchant;

public class WomboComboEnchant extends StackedArmorMetadataEnchant {

	public WomboComboEnchant() {
		super("WomboCombo", 1, 0.65);
	}

	@Override
	public EnchantmentCategory getCategory() {
		return EnchantmentCategory.ARMOR;
	}
	
	@EventHandler
	public void EntityDamageByEntity(EntityDamageByEntityEvent event) {
		
		if (event.getDamager() instanceof Player) {
			
			Player p = (Player) event.getDamager();
			
			if (hasEffect(p)) {
				
				// 1 to 2 hearts
				int damage = new Random().nextInt(3) + 2;
				double chance = getChance(0.15);
				if (new Random().nextDouble() <= chance)
					SkyEnchant.getInstance().getServer().getScheduler().runTaskLater(SkyEnchant.getInstance(),
							new DelayedHitRunnable((Damageable) event.getEntity(), damage), 15L);
				
			}
		}
	}
	
	public class DelayedHitRunnable implements Runnable {
		
		private int damage;
		private Damageable damaged;
		
		public DelayedHitRunnable(Damageable damaged, int damage) {
			this.damaged = damaged;
			this.damage = damage;
		}
		
		public void run() {
			damaged.getWorld().playEffect(damaged.getLocation(), Effect.CRIT, 4);
			damaged.playEffect(EntityEffect.HURT);
			damaged.setHealth(damaged.getHealth() - damage);
		}
		
	}
	
}
